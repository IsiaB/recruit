@extends('layouts.app')

@section('title', 'Create candidate') <!-- :סקשן פשוט ולכן לא מצריך תו סגירה, שם ללשונית של הדפדפן-->

@section('content')

            <h1> Create candidates </h1> <!-- מרכוז הכותרת-->
            <form method = "post" action = "{{action('CandidatesController@store')}}">
            @csrf <!--אבטחת מידע-->

            <div class="form-group">
                <label for = "name" >Candidates name </label>
                <input type = "text" class="form-control" name = "name">
            </div>
            <div class="form-group">
                <label for = "email">Candidates email </label>
                <input type = "text" class="form-control" name = "email">
                
            </div>
            <div class="form-group">
                <input type = "submit" class="btn btn-info" name = "submit" value = "Create candidate" >
            </div>

            </form>        
 @endsection
