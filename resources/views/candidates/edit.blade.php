@extends('layouts.app')

@section('title', 'Edit candidate')

@section('content')

    <form method = "post" action = "{{action('CandidatesController@update', $candidate->id)}}">
        @method('PATCH')
        @csrf
        <div class="form-group">
            <label for = "name">Candidate name</label>
            <input type = "text" class="form-control" name = "name" value = {{$candidate->name}}>
        </div>
        <div class="form-group">
            <label for = "name" >Candidate email</label>
            <input type = "text" class="form-control" name = "email" value = {{$candidate->email}}>
        </div>
        <div class="form-group">
            <input type = "submit" class="btn btn-success" name = "submit" value = "Update candidate">
        </div>
    </form> 
     
@endsection        
                    

