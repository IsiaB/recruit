<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint; ##לא בטוח שצריך

class Status extends Model
{
    

    protected $fillable = [
        'name', 
    ];

    public function candidates(){
        return $this->hasMany('App\Candidate');
    }

    public static function next($status_id){ //פונקציה שמאפשר לי לעבור מססטוס מסויים לסטטוס מסויים
        $nextstages = DB::table('nextstages')->where('from', $status_id)->pluck('to');//
        return self::find($nextstages)->all(); //כיוון שאנחנו עושים שימוש בפוציה ששייך למודל הנוכחי שאנו נמצאים בו self נבצע שימו ב:
        //האינפוט זה הסטטוס שקיים והאווטפוט זה הסטטוס שאליו אני רוצה לעבור 
        //בחלק הזה לאחר שיש לנו את האיידי של הסטטוסים שאליהם ניתן לעבור אנחנו שולחים את הססטוס עצמו את האיידי
    }

    public static function allowed($from,$to){
        $allowed = DB::table('nextstages')->where('from',$from)->where('to',$to)->get();
        if(isset($allowed)) return true;
        return false;
        
        }
        
    
} 