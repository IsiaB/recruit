<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $fillable=['name','email'];

    public function owner(){  //השם של הפונקציה במידה והוא היה יוסר לא היינו צריכים לכתוב יוסר קו תחתון איידי
        return $this->belongsTo('App\User','user_id'); //יצירת קשר בין candidate לuser כל קנדידט שייך למשתמש כלשהו
    }

    public function status(){ //בגלל שהשם הוא סטטוס אין צורך לכתוב סטטוס איידי
        return $this->belongsTo('App\Status'); 
    }
    
}